---
title: Buttons
---

<form action="# method="get">
  <h2>Native Buttons</h2>
  <h3>Singleton</h3>
  <p>Distinguish from background.</p>
  <button type="button">Generic</button>


<h3>Group</h3>
<p>Distinguish from background, siblings, and sibling states</p>
<section>
  <h4>Types</h4>
  <button type="submit">Submit</button>
  <button type="button">Generic</button>
  <button type="reset">Reset</button>
</section>
<section>
<style>
  .active {}
  .focus {}
  .hover {}
</style>
<h4>States</h4>
<button type="button">Generic</button>
<button type="button" disabled>Disabled</button>  
<button type="button" class="focus">Focus</button>
<button type="button" class="hover">Hover</button>
<button type="button" class="active">Active/Pressed</button>
</section>

  <h3>Distinguishing characteristics</h3>
  <ul>
    <li>Font Family</li>
    <li>Color</li>
    <li>Color Contrast</li>
    <li>Background gradient</li>
    <li>Border</li>
    <li>Cursor</li>
  </ul>

</form>
